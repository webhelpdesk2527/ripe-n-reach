/**
 * Elements that make up the popup.
 */
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');

/**
 * Create an overlay to anchor the popup to the map.
 */
var overlay = new ol.Overlay({
    element: container,
    autoPan: true,
    autoPanAnimation: {
        duration: 250
    }
});

/**
 * Add a click handler to hide the popup.
 * @return {boolean} Don't follow the href.
 */
closer.onclick = function() {
    overlay.setPosition(undefined);
    closer.blur();
    return false;
};

var map = new ol.Map({
    view: new ol.View({
        center: [9702648.840921275, 2590431.779327552],
        zoom: 12
    }),
    layers: [
        new ol.layer.Tile({
            source: new ol.source.OSM()
        })
    ],
    target: 'map',
    overlays: [overlay]
});

var geolocation = new ol.Geolocation();
geolocation.setTracking(true); // here the browser may ask for confirmation
geolocation.on('change:position', function() {
    console.log(geolocation.getPosition());
});

var coldStorageSource = new ol.source.Vector({
    format: new ol.format.GeoJSON(),
    url: '/data/cold-storage.json'
});
var coldStorageLayer = new ol.layer.Vector({
    source: coldStorageSource,
    style: new ol.style.Style({
        image: new ol.style.Circle({
            radius: 8,
            fill: new ol.style.Fill({
                color: [111, 18, 204, 0]
            }),
            stroke: new ol.style.Stroke({ color: 'rgba(145, 0, 122, 0)', width: 4 })
        })
    }),
    renderBuffer: 200,
    renderMode: 'image'
});
coldStorageLayer.set('name', 'cold-storages');
map.addLayer(coldStorageLayer);

var marketSource = new ol.source.Vector({
    format: new ol.format.GeoJSON(),
    url: '/data/market.json'
});
var marketLayer = new ol.layer.Vector({
    source: marketSource,
    style: new ol.style.Style({
        image: new ol.style.Circle({
            fill: new ol.style.Fill({
                color: [111, 18, 204, 0]
            }),
            stroke: new ol.style.Stroke({ color: 'rgba(148, 0, 211, 01)', width: 4 })
        })
    }),
    renderBuffer: 200,
    renderMode: 'image'
});
marketLayer.set('name', 'markets')
map.addLayer(marketLayer);

var _drawsource = new ol.source.Vector({
    wrapX: false
});

var _buffersource = new ol.source.Vector({
    wrapX: false
});
var bufferLayer = new ol.layer.Vector({
    source: _buffersource,
    style: new ol.style.Style({
        stroke: new ol.style.Stroke({
            color: 'blue',
            width: 1
        }),
        fill: new ol.style.Fill({
            color: 'rgba(255, 0, 0, 0.2)'
        })
    }),
    renderBuffer: 200,
    renderMode: 'image'
});
map.addLayer(bufferLayer);

var styles = {
    'market': [new ol.style.Style({
        image: new ol.style.Icon({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: '/img/market.png'
        })
    })],
    'cold-storage': [new ol.style.Style({
        image: new ol.style.Icon({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            src: '/img/cold-storage.png'
        })
    })],
};
var _vectorSource = new ol.source.Vector({
    wrapX: false
});
var _vlayer = new ol.layer.Vector({
    source: _vectorSource,
    style: function(feature, resolution) {
        return styles[feature.getProperties()["category"]];
    },
    renderBuffer: 200,
    renderMode: 'image'
});
_vlayer.set('name', 'search-result')
map.addLayer(_vlayer);

map.on('singleclick', function(evt) {
    console.log(evt.coordinate)
    var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature) {
        document.getElementById('_name').innerHTML = feature.getProperties()["name"];
        document.getElementById('_address').innerHTML = feature.getProperties()["address"];
        overlay.setPosition(evt.coordinate);
    }, {
        layerFilter: function(layer) {
            return layer.get('name') === 'search-result';
        }
    });
});

jQuery('.btn-search').on('click', function(e){
    _drawsource.clear()
    bufferLayer.getSource().clear()
    _vlayer.getSource().clear()

    _drawvector = new ol.layer.Vector({
        source: _drawsource
    });
    _drawvector.set('name', 'drawnGeom');
    map.addLayer(_drawvector);
    
    var _attrdraw = new ol.interaction.Draw({
        source:_drawsource,
        type: 'Point'
    });
    map.addInteraction(_attrdraw);
    _attrdraw.on('drawend', function (evt) {
        map.removeInteraction(_attrdraw);
        var _fgeom = evt.feature.getGeometry();
        initSearchFunction(_fgeom);
    },
    this);
});

function initSearchFunction(_fgeom) {
    selectedFeatures = [];
    var coordinate = _fgeom.getCoordinates();
    var buffer = new ol.Feature(new ol.geom.Circle(coordinate, parseInt($('#dist').children("option:selected").val())));
    bufferLayer.getSource().addFeature(buffer);

    if(jQuery("input[name=layertype]:checked").val() == 'market') {
        marketSource.forEachFeatureInExtent(buffer.getGeometry().getExtent(),function(feature){
            selectedFeatures.push(feature);
        });
    } else if(jQuery("input[name=layertype]:checked").val() == 'coldStorage') {
        coldStorageSource.forEachFeatureInExtent(buffer.getGeometry().getExtent(),function(feature){
            selectedFeatures.push(feature);
        });
    }
    _vlayer.getSource().addFeatures(selectedFeatures);
}