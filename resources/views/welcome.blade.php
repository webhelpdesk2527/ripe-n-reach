<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css">
        <link rel="stylesheet" href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css"
    type="text/css">
        
        <!-- Scripts -->
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" ></script>
        <script src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
        <script src="https://kit.fontawesome.com/a786604bcc.js"></script>
    </head>
    <body>
        <div id="app">
            <div class="sideBar">
                <img src="{{ asset('img/logo.svg') }}" alt="Ripe n Reach logo" class="logo">
                <div class="scrollArea">
                    <div id="accordion">
                        <div>
                            <a href="#" class="optionHead" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Looking To Sell <i class="fas fa-chevron-down"></i></a>

                            <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                <div class="form-group d-flex justify-content-around">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="layertype" id="market" value="market" checked>
                                        <label class="form-check-label" for="market">Market</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="layertype" id="coldStorage" value="coldStorage">
                                        <label class="form-check-label" for="coldStorage">Cold Storage</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="dist"><strong>Distance</strong></label>
                                    <select class="form-control" id="dist" name="dist">
                                        <option value=5000>Near Me</option>
                                        <option value=10000>Within 10 km</option>
                                        <option value=20000>Within 20 km</option>
                                        <option value=30000>Within 30 km</option>
                                        <option value=40000>Within 40 km</option>
                                        <option value=50000 selected>Within 50 km</option>
                                    </select>
                                </div>
                                <button class="btn btn-success btn-search btn-sm badge-pill btn-block">Place Point and Search</button>
                                <hr>
                            </div>
                        </div>
                        
                        <div>
                            <a href="#" class="optionHead collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Feeling Sick <i class="fas fa-chevron-down"></i></a>

                            <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                <div class="form-group d-flex justify-content-around">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="emergencytype" id="hospital" value="hospital" checked>
                                        <label class="form-check-label" for="market">Hospital</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="emergencytype" id="doctor" value="doctor">
                                        <label class="form-check-label" for="doctor">Doctor</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="emergencytype" id="pharmacy" value="pharmacy">
                                        <label class="form-check-label" for="pharmacy">Pharmacy</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="dist"><strong>Distance</strong></label>
                                    <select class="form-control" id="dist" name="dist">
                                        <option value=5000>Near Me</option>
                                        <option value=10000>Within 10 km</option>
                                        <option value=20000>Within 20 km</option>
                                        <option value=30000>Within 30 km</option>
                                        <option value=40000>Within 40 km</option>
                                        <option value=50000>Within 50 km</option>
                                    </select>
                                </div>
                                <button class="btn btn-success btn-filling-sick btn-sm badge-pill btn-block">Search</button>
                                <hr>
                            </div>
                        </div>
                        
                        <div>
                            <a href="#" class="optionHead collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Need Staff For Farm Work <i class="fas fa-chevron-down"></i></a>

                            <div id="collapseThree" class="collapse" data-parent="#accordion">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="stafftype" id="fruit" value="fruit" checked>
                                        <label class="form-check-label" for="market">Fruit Picking</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="stafftype" id="ploughing" value="ploughing">
                                        <label class="form-check-label" for="doctor">Ploughing</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="stafftype" id="animal" value="animal">
                                        <label class="form-check-label" for="pharmacy">Animal & Husbandry Care</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="stafftype" id="logistics" value="logistics">
                                        <label class="form-check-label" for="pharmacy">Logistics & Transportation</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="stafftype" id="other" value="other">
                                        <label class="form-check-label" for="pharmacy">Others</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="dist"><strong>Number of staffs you want</strong></label>
                                    <input type="number" class="form-control" min="1" step="1">
                                </div>
                                <button class="btn btn-success btn-filling-sick btn-sm badge-pill btn-block">Send Hiring Request to Executive</button>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="map" class="map"></div>
            <div id="popup" class="ol-popup">
                <a href="#" id="popup-closer" class="ol-popup-closer"></a>
                <div id="popup-content">
                    <h5>Name: <span id="_name"></span></h5>
                    <p>Address: <span id="_address"></span></p>
                    <button class="btn btn-small">Contact</button>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/ripe-n-reach.js') }}"></script>
    </body>
</html>
